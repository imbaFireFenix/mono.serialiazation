﻿namespace Unknown.Mono.System.Runtime.Remoting.Messaging
{
    public class IInternalMessage
    {
        #region Fields
            static private global::System.Type _Type;

            static private global::System.Reflection.PropertyInfo __Uri;
        #endregion

        static IInternalMessage()
        {
            Unknown.Mono.System.Runtime.Remoting.Messaging.IInternalMessage._Type = global::System.Type.GetType("System.Runtime.Remoting.Messaging.IInternalMessage");

            Unknown.Mono.System.Runtime.Remoting.Messaging.IInternalMessage.__Uri = Unknown.Mono.System.Runtime.Remoting.Messaging.IInternalMessage._Type.GetProperty("Uri", global::System.Reflection.BindingFlags.GetProperty | global::System.Reflection.BindingFlags.SetProperty);
        }

        static public void Uri(global::System.Runtime.Remoting.Messaging.IMethodMessage Message, string Value)
        {
            Unknown.Mono.System.Runtime.Remoting.Messaging.IInternalMessage.__Uri.SetValue(Message, Value, null);
        }
    }
}