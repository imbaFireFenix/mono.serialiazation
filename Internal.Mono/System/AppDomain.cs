﻿namespace Unknown.Mono.System
{
    public class AppDomain
    {
        #region Fields
            static private global::System.Reflection.MethodInfo __DefineInternalDynamicAssembly;
        #endregion

        static AppDomain()
        {
            Unknown.Mono.System.AppDomain.__DefineInternalDynamicAssembly = typeof(global::System.AppDomain).GetMethod("DefineInternalDynamicAssembly");
        }

        static public global::System.Reflection.Emit.AssemblyBuilder DefineInternalDynamicAssembly(global::System.AppDomain AppDomain, global::System.Reflection.AssemblyName Name, global::System.Reflection.Emit.AssemblyBuilderAccess Access)
        {
            return (global::System.Reflection.Emit.AssemblyBuilder)Unknown.Mono.System.AppDomain.__DefineInternalDynamicAssembly.Invoke(AppDomain, new object[] {Name, Access});
        }
    }
}