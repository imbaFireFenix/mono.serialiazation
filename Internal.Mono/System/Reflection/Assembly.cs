﻿namespace Unknown.Mono.System.Reflection
{
    public class Assembly
    {
        #region Fields
            static private global::System.Reflection.PropertyInfo __GrantedPermissionSet;
        #endregion

        static Assembly()
        {
            Unknown.Mono.System.Reflection.Assembly.__GrantedPermissionSet = typeof(global::System.Reflection.Assembly).GetProperty("GrantedPermissionSet", global::System.Reflection.BindingFlags.GetProperty);
        }

        static public global::System.Security.PermissionSet GrantedPermissionSet(global::System.Reflection.Assembly Assembly)
        {
            return (global::System.Security.PermissionSet)(Unknown.Mono.System.Reflection.Assembly.__GrantedPermissionSet.GetValue(Assembly, null));
        }
    }
}