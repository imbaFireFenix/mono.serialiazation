﻿namespace Unknown.Mono
{
    public class Types
    {
        #region Fields
            static public global::System.Type MonoType
            {
                get;
                private set;
            }

            static public global::System.Type MonoTypeArray
            {
                get;
                private set;
            }
        #endregion

        static Types()
        {
            Unknown.Mono.Types.MonoType      = global::System.Type.GetType("System.MonoType");
            Unknown.Mono.Types.MonoTypeArray = global::System.Type.GetType("System.MonoType[]");
        }
    }
}