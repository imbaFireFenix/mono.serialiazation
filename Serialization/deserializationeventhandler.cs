// ==++==
// 
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Interface: DeserializationEventHandler
**
**
** Purpose: The multicast delegate called when the DeserializationEvent is thrown.
**
**
===========================================================*/
namespace Unknown.Serialization {

    [System.Serializable]
    internal delegate void DeserializationEventHandler(System.Object sender);

    [System.Serializable]
    internal delegate void SerializationEventHandler(StreamingContext context);
    
}
