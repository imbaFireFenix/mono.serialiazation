﻿// ==++==
// 
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Class: FormatterServices
**
**
** Purpose: Provides some static methods to aid with the implementation
**          of a Formatter for Serialization.
**
**
============================================================*/
namespace Unknown.Serialization {
    
    using System;
    using System.Reflection;
    using System.Collections;
    using System.Collections.Generic;
    using System.Security;    
    using System.Security.Permissions;
    using Unknown.Serialization.Formatters;
    using System.Runtime.Remoting;
    using System.Runtime.CompilerServices;
    using System.Runtime.Versioning;
    using System.Threading;
    using System.IO;
    using System.Text;
    using System.Globalization;

    [System.Runtime.InteropServices.ComVisible(true)]
    public static class FormatterServices {     
        internal static Dictionary<MemberHolder, MemberInfo[]> m_MemberInfoTable = new Dictionary<MemberHolder, MemberInfo[]>(32);
        [System.Security.SecurityCritical]
        private static bool unsafeTypeForwardersIsEnabled = false;

        [System.Security.SecurityCritical]
        private static volatile bool unsafeTypeForwardersIsEnabledInitialized = false;

        private static Object s_FormatterServicesSyncObject = null;

        private static Object formatterServicesSyncObject
        {
            get
            {
                if (s_FormatterServicesSyncObject == null)
                {
                    Object o = new Object();
                    Interlocked.CompareExchange<Object>(ref s_FormatterServicesSyncObject, o, null);
                }
                return s_FormatterServicesSyncObject;
            }
        }

        [SecuritySafeCritical]
        static FormatterServices()
        {
            // Static initialization touches security critical types, so we need an
            // explicit static constructor to allow us to mark it safe critical.
        }

        private static MemberInfo[] GetSerializableMembersRuntime(Type type)
        {
            // get the list of all fields
            FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            int countProper = 0;
            for (int i = 0;  i < fields.Length; i++) {
                if ((fields[i].Attributes & FieldAttributes.NotSerialized) == FieldAttributes.NotSerialized)
                    continue;
                countProper++;
            }
            if (countProper != fields.Length) {
                FieldInfo[] properFields = new FieldInfo[countProper];
                countProper = 0;
                for (int i = 0;  i < fields.Length; i++) {
                    if ((fields[i].Attributes & FieldAttributes.NotSerialized) == FieldAttributes.NotSerialized)
                        continue;
                    properFields[countProper] = fields[i];
                    countProper++;
                }
                return properFields;
            }
            else
                return fields;
        }

        private static bool CheckSerializable(Type type) {
            if (type.IsSerializable) {
                return true;
            }
            return false;
        }

        private static MemberInfo[] InternalGetSerializableMembers(Type type) {
            List<SerializationFieldInfo> allMembers = null;
            MemberInfo[] typeMembers;
            FieldInfo [] typeFields;
            Type parentType;
            
            //<

            if (type.IsInterface) {
                return new MemberInfo[0];
            }

            if (!(CheckSerializable(type))) {
                    throw new global::System.Runtime.Serialization.SerializationException("Serialization_NonSerType: " + type.FullName + " " + type.Module.Assembly.FullName);
            }
          
            //Get all of the serializable members in the class to be serialized.
            typeMembers = GetSerializableMembersRuntime(type);

            //If this class doesn't extend directly from object, walk its hierarchy and 
            //get all of the private and assembly-access fields (e.g. all fields that aren't
            //virtual) and include them in the list of things to be serialized.  
            parentType = (Type)(type.BaseType);
            if (parentType != null && parentType != typeof(Object)) {
                Type[] parentTypes = null;
                int parentTypeCount = 0;
                bool classNamesUnique = GetParentTypes(parentType, out parentTypes, out parentTypeCount);
                if (parentTypeCount > 0){
                    allMembers = new List<SerializationFieldInfo>();
                    for (int i = 0; i < parentTypeCount;i++){
                        parentType = parentTypes[i];
                        if (!CheckSerializable(parentType)) {
                                throw new global::System.Runtime.Serialization.SerializationException("Serialization_NonSerType:" + parentType.FullName + " " + parentType.Module.Assembly.FullName);
                        }

                        typeFields = parentType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
                        String typeName = classNamesUnique ? parentType.Name : parentType.FullName;
                        foreach (FieldInfo field in typeFields) {
                            // Family and Assembly fields will be gathered by the type itself.
                            if (!field.IsNotSerialized) {
                                allMembers.Add(new SerializationFieldInfo(field, typeName));
                            }
                        }
                    }
                    //If we actually found any new MemberInfo's, we need to create a new MemberInfo array and
                    //copy all of the members which we've found so far into that.
                    if (allMembers!=null && allMembers.Count>0) {
                        MemberInfo[] membersTemp = new MemberInfo[allMembers.Count + typeMembers.Length];
                        Array.Copy(typeMembers, membersTemp, typeMembers.Length);
                        ((ICollection)allMembers).CopyTo(membersTemp, typeMembers.Length);
                        typeMembers = membersTemp;
                    }
                }
            }
            return typeMembers;
        }

        private static bool GetParentTypes(Type parentType, out Type[] parentTypes, out int parentTypeCount){
            //Check if there are any dup class names. Then we need to include as part of
            //typeName to prefix the Field names in SerializationFieldInfo
            /*out*/ parentTypes = null;
            /*out*/ parentTypeCount = 0;
            bool unique = true;
            Type objectType = typeof(object);
            for (Type t1 = parentType; t1 != objectType; t1 = t1.BaseType)
            {
                if (t1.IsInterface) continue;
                string t1Name = t1.Name;
                for(int i=0;unique && i<parentTypeCount;i++){
                    string t2Name = parentTypes[i].Name;
                    if (t2Name.Length == t1Name.Length && t2Name[0] == t1Name[0] && t1Name == t2Name){
                        unique = false;
                        break;
                    }
                }
                //expand array if needed
                if (parentTypes == null || parentTypeCount == parentTypes.Length){
                    Type[] tempParentTypes = new Type[Math.Max(parentTypeCount*2, 12)];
                    if (parentTypes != null)
                        Array.Copy(parentTypes, 0, tempParentTypes, 0, parentTypeCount);
                    parentTypes = tempParentTypes;
                }
                parentTypes[parentTypeCount++] = t1;
            }
            return unique;
        }

        // Get all of the Serializable members for a particular class.  For all practical intents and
        // purposes, this is the non-transient, non-static members (fields and properties).  In order to
        // be included, properties must have both a getter and a setter.  N.B.: A class
        // which implements ISerializable or has a serialization surrogate may not use all of these members
        // (or may have additional members).
        [System.Security.SecurityCritical]  // auto-generated_required
        public static MemberInfo[] GetSerializableMembers(Type type) {
            return GetSerializableMembers(type, new StreamingContext(StreamingContextStates.All));
        }

        // Get all of the Serializable Members for a particular class.  If we're not cloning, this is all
        // non-transient, non-static fields.  If we are cloning, include the transient fields as well since
        // we know that we're going to live inside of the same context.
        [System.Security.SecurityCritical]  // auto-generated_required
        public static MemberInfo[] GetSerializableMembers(Type type, StreamingContext context) {
            MemberInfo[] members;
    
            if ((object)type==null) {
                throw new ArgumentNullException("type");
            }
            

            //if (!(type is RuntimeType)) {
            //    throw new global::System.Runtime.Serialization.SerializationException("Serialization_InvalidType:" + type.ToString()));
            //}
    
            MemberHolder mh = new MemberHolder(type, context);
    
            //If we've already gathered the members for this type, just return them.
            if (m_MemberInfoTable.ContainsKey(mh)) {
                return m_MemberInfoTable[mh];
            }
            
            lock (formatterServicesSyncObject) {
                //If we've already gathered the members for this type, just return them.
                if (m_MemberInfoTable.ContainsKey(mh)) {
                    return m_MemberInfoTable[mh];
                }

                members = InternalGetSerializableMembers(type);
            
                m_MemberInfoTable[mh] = members;
            }
    
            return members;
        }
      
        static readonly Type[] advancedTypes = new Type[]{
            /*typeof(System.DelegateSerializationHolder),*/ Unknown.Mono.System.DelegateSerializationHolder.Type,
#if FEATURE_REMOTING                        
            typeof(System.Runtime.Remoting.ObjRef),
            typeof(System.Runtime.Remoting.IEnvoyInfo),
            typeof(System.Runtime.Remoting.Lifetime.ISponsor),
#endif            
        };

        public static void CheckTypeSecurity(Type t, TypeFilterLevel securityLevel)
        {
            if (securityLevel == TypeFilterLevel.Low)
            {
                for (int i = 0; i < advancedTypes.Length; i++)
                {
                    if (advancedTypes[i].IsAssignableFrom(t))
                    {
                        throw new SecurityException("Serialization_TypeSecurity:" + advancedTypes[i].FullName + " " + t.FullName);
                    }
                }
            }
        }

        // Gets a new instance of the object.  The entire object is initalized to 0 and no 
        // constructors have been run. **THIS MEANS THAT THE OBJECT MAY NOT BE IN A STATE
        // CONSISTENT WITH ITS INTERNAL REQUIREMENTS** This method should only be used for
        // deserialization when the user intends to immediately populate all fields.  This method
        // will not create an unitialized string because it is non-sensical to create an empty
        // instance of an immutable type.
        //
        [System.Security.SecurityCritical]  // auto-generated_required
        public static Object GetUninitializedObject(Type type) {
            if ((object)type == null) {
                throw new ArgumentNullException("type");
            }
            
    
            //if (!(type is RuntimeType)) {
            //    throw new global::System.Runtime.Serialization.SerializationException("Serialization_InvalidType: " + type.ToString()));
            //}

            return global::System.Runtime.Serialization.FormatterServices.GetUninitializedObject(type); // nativeGetUninitializedObject((RuntimeType)type);
        }
    
        [System.Security.SecurityCritical]  // auto-generated_required
        public static Object GetSafeUninitializedObject(Type type) {
             if ((object)type == null) {
                throw new ArgumentNullException("type");
            }
             
    
            //if (!(type is RuntimeType)) {
            //    throw new global::System.Runtime.Serialization.SerializationException("Serialization_InvalidType:" + type.ToString()));
            //}
#if FEATURE_REMOTING            
            if (Object.ReferenceEquals(type, typeof(System.Runtime.Remoting.Messaging.ConstructionCall)) || 
                Object.ReferenceEquals(type, typeof(System.Runtime.Remoting.Messaging.LogicalCallContext)) ||
                Object.ReferenceEquals(type, typeof(System.Runtime.Remoting.Contexts.SynchronizationAttribute)))
                 return nativeGetUninitializedObject((RuntimeType)type);                    
#endif

            try {                            
                return global::System.Runtime.Serialization.FormatterServices.GetSafeUninitializedObject(type); //return nativeGetSafeUninitializedObject((RuntimeType)type);                    
            }
            catch(SecurityException e) {                
                throw new global::System.Runtime.Serialization.SerializationException("Serialization_Security:" +  type.FullName);
            } 
        }

        private static Binder s_binder = Type.DefaultBinder;
        [System.Security.SecurityCritical]
        internal static void SerializationSetValue(MemberInfo fi, Object target, Object value)
        {
            FieldInfo serField = fi as FieldInfo;

            if (serField != null)
            {
                serField.SetValue(target, value, BindingFlags.Default, s_binder, null);
                return;
            }

            throw new global::System.ArgumentException("Argument_InvalidFieldInfo");
        }

        // Fill in the members of obj with the data contained in data.
        // Returns the number of members populated.
        //
        [System.Security.SecurityCritical]  // auto-generated_required
        public static Object PopulateObjectMembers(Object obj, MemberInfo[] members, Object[] data) {
            if (obj==null) {
                throw new ArgumentNullException("obj");
            }

            if (members==null) {
                throw new ArgumentNullException("members");
            }

            if (data==null) {
                throw new ArgumentNullException("data");
            }

            if (members.Length!=data.Length) {
                throw new global::System.ArgumentException("Argument_DataLengthDifferent");
            }

            MemberInfo mi;

            for (int i=0; i<members.Length; i++) {
                mi = members[i];
    
                if (mi==null) {
                    throw new ArgumentNullException("ArgumentNull_NullMember:" + i);
                }
        
                //If we find an empty, it means that the value was never set during deserialization.
                //This is either a forward reference or a null.  In either case, this may break some of the
                //invariants mantained by the setter, so we'll do nothing with it for right now.
                if (data[i]!=null)
                {
                    if (mi.MemberType==MemberTypes.Field)
                     {
                        SerializationSetValue(mi, obj, data[i]);
                    } else {
                        throw new global::System.Runtime.Serialization.SerializationException("Serialization_UnknownMemberInfo");
                    }
                }
                //Console.WriteLine("X");
            }
            
            

            return obj;
        }
    
        // Extracts the data from obj.  members is the array of members which we wish to
        // extract (must be FieldInfos or PropertyInfos).  For each supplied member, extract the matching value and
        // return it in a Object[] of the same size.
        //
        [System.Security.SecurityCritical]  // auto-generated_required
        public static Object[] GetObjectData(Object obj, MemberInfo[] members) {
    
            if (obj==null) {
                throw new ArgumentNullException("obj");
            }
    
            if (members==null) {
                throw new ArgumentNullException("members");
            }
            
            
            int numberOfMembers = members.Length;
    
            Object[] data = new Object[numberOfMembers];
            MemberInfo mi;
    
            for (int i=0; i<numberOfMembers; i++) {
                mi=members[i];
    
                if (mi==null)
                {
                    throw new ArgumentNullException("ArgumentNull_NullMember:" + i);
                }
    
                if (mi.MemberType==MemberTypes.Field)
                {
                    data[i] = ((FieldInfo)mi).GetValue(obj);
                } else {
                    throw new global::System.Runtime.Serialization.SerializationException("Serialization_UnknownMemberInfo");
                }
            }
    
            return data;
        }

        [System.Security.SecurityCritical]  // auto-generated_required
        [System.Runtime.InteropServices.ComVisible(false)]
        public static ISerializationSurrogate GetSurrogateForCyclicalReference(ISerializationSurrogate innerSurrogate)
        {
            if (innerSurrogate == null)
                throw new ArgumentNullException("innerSurrogate");
            
            return new SurrogateForCyclicalReference(innerSurrogate);
        }

        /*=============================GetTypeFromAssembly==============================
        **Action:
        **Returns:
        **Arguments:
        **Exceptions:
        ==============================================================================*/
        [System.Security.SecurityCritical]  // auto-generated_required
        public static Type GetTypeFromAssembly(Assembly assem, String name) {
            if (assem==null)
                throw new ArgumentNullException("assem");
            
            return assem.GetType(name, false, false);
        }
    
        /*============================LoadAssemblyFromString============================
        **Action: Loads an assembly from a given string.  The current assembly loading story
        **        is quite confusing.  If the assembly is in the fusion cache, we can load it
        **        using the stringized-name which we transmitted over the wire.  If that fails,
        **        we try for a lookup of the assembly using the simple name which is the first
        **        part of the assembly name.  If we can't find it that way, we'll return null
        **        as our failure result.
        **Returns: The loaded assembly or null if it can't be found.
        **Arguments: assemblyName -- The stringized assembly name.
        **Exceptions: None
        ==============================================================================*/
        internal static Assembly LoadAssemblyFromString(String assemblyName) {
            //
            // Try using the stringized assembly name to load from the fusion cache.
            //
            
            Assembly found = Assembly.Load(assemblyName);
            return found;
        }

        internal static Assembly LoadAssemblyFromStringNoThrow(String assemblyName) {
            try {
                return LoadAssemblyFromString(assemblyName);
            }
            catch (Exception e){
                
            }
            return null;
        }

        internal static string GetClrAssemblyName(Type type, out bool hasTypeForwardedFrom) {
            if ((object)type == null) {
                throw new ArgumentNullException("type");
            }

            hasTypeForwardedFrom = false;
            return type.Assembly.FullName;
        }

        internal static string GetClrTypeFullName(Type type) {
            if (type.IsArray) {
                return GetClrTypeFullNameForArray(type);
            }
            else {
                return GetClrTypeFullNameForNonArrayTypes(type);
            }
        }

        static string GetClrTypeFullNameForArray(Type type) {
            int rank = type.GetArrayRank();
            if (rank == 1)
            {
                return String.Format(CultureInfo.InvariantCulture, "{0}{1}", GetClrTypeFullName(type.GetElementType()), "[]");
            }
            else
            {
                StringBuilder builder = new StringBuilder(GetClrTypeFullName(type.GetElementType())).Append("[");
                for (int commaIndex = 1; commaIndex < rank; commaIndex++)
                {
                    builder.Append(",");
                }
                builder.Append("]");
                return builder.ToString();
            }
        }

        static string GetClrTypeFullNameForNonArrayTypes(Type type) {
            if (!type.IsGenericType) {
                return type.FullName;
            }

            Type[] genericArguments = type.GetGenericArguments();
            StringBuilder builder = new StringBuilder(type.GetGenericTypeDefinition().FullName).Append("[");
            bool hasTypeForwardedFrom;

            foreach (Type genericArgument in genericArguments) {
                builder.Append("[").Append(GetClrTypeFullName(genericArgument)).Append(", ");
                builder.Append(GetClrAssemblyName(genericArgument, out hasTypeForwardedFrom)).Append("],");
            }

            //remove the last comma and close typename for generic with a close bracket
            return builder.Remove(builder.Length - 1, 1).Append("]").ToString();
        }
    }

    internal sealed class SurrogateForCyclicalReference : ISerializationSurrogate
    {
        ISerializationSurrogate innerSurrogate;
        internal SurrogateForCyclicalReference(ISerializationSurrogate innerSurrogate)
        {
            if (innerSurrogate == null)
                throw new ArgumentNullException("innerSurrogate");
            this.innerSurrogate = innerSurrogate;
        }

        [System.Security.SecurityCritical]  // auto-generated        
        public void GetObjectData(Object obj, SerializationInfo info, StreamingContext context)
        {
            innerSurrogate.GetObjectData(obj, info, context);
        }
        
        [System.Security.SecurityCritical]  // auto-generated
        public Object SetObjectData(Object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            return innerSurrogate.SetObjectData(obj, info, context, selector);
        }
    }
}





