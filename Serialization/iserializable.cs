// ==++==
// 
//   Copyright (c) Microsoft Corporation.  All rights reserved.
// 
// ==--==
/*============================================================
**
** Interface: ISerializable
**
**
** Purpose: Implemented by any object that needs to control its
**          own serialization.
**
**
===========================================================*/

namespace Unknown.Serialization {
    using System.Runtime.Remoting;
    using Unknown.Serialization;
    using System.Security.Permissions;
    using System;
    using System.Reflection;

    [System.Runtime.InteropServices.ComVisible(true)]
    public interface ISerializable {
        [System.Security.SecurityCritical]  // auto-generated_required
        void GetObjectData(SerializationInfo info, StreamingContext context);
    }

}




